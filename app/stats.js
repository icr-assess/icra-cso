// code involved in reporting on the student's ICR assessment

const ICRTARGET = 600; // msec below which is considered ICR

function analyseICRAData(srcData){
    /* analyze data on student's performance on this assessment:
      > percent correct
      > average, min, and max response time (correct characters only)
      > percent icr (correct characters only)
      > in addition the analysis will produce an array (details) listing info 
        for each character which can be used for sorting, etc.
      srcData is the contents of assessmentData from sketch.js
    */
    
    let correctChars = 0;
    let maxTime = 0;
    let minTime = 100000;
    let totalTime = 0;
    let icrChars = 0;
    let details = [];

    numChars = srcData.length;

    for (var charIndex = 0; charIndex < numChars; charIndex++){
      currChar = srcData[charIndex].trueChar;
      currResponse = srcData[charIndex].responseTime;
      currTyped = srcData[charIndex].charTyped;
      if (currChar == currTyped){
          // answer was correct - include info in assessment totals
          correctChars += 1;
          totalTime += currResponse;
          if (currResponse < minTime){
            minTime = currResponse;
          };
          if (currResponse > maxTime){
            maxTime = currResponse;
          };
          isICR = false;
          if (currResponse <= ICRTARGET){
            isICR = true;
            icrChars += 1;
          }
        details.push([currChar, "Correct", currResponse, isICR]);
        } else {
          // answer was incorrect - exclude from stats
        details.push([currChar, "Incorrect", currResponse, false]);
        }
    }
    // now compute statistics
    stats = {
      details: details,
      percentCorrect: correctChars / numChars * 100.,
      percentICR: icrChars / numChars * 100.,
      averageTime: totalTime / correctChars,
      maxTime: maxTime,
      minTime: minTime
    }
    return stats
  }