// generate cw signals for icra

const MORSE = {
    "a": ".-",
    "b": "-...",
    "c": "-.-.",
    "d": "-..",
    "e": ".",
    "f": "..-.",
    "g": "--.",
    "h": "....",
    "i": "..",
    "j": ".---",
    "k": "-.-",
    "l": ".-..",
    "m": "--",
    "n": "-.",
    "o": "---",
    "p": ".--.",
    "q": "--.-",
    "r": ".-.",
    "s": "...",
    "t": "-",
    "u": "..-",
    "v": "...-",
    "w": ".--",
    "x": "-..-",
    "y": "-.--",
    "z": "--..",
    "0": "-----",
    "1": ".----",
    "2": "..---",
    "3": "...--",
    "4": "....-",
    "5": ".....",
    "6": "-....",
    "7": "--...",
    "8": "---..",
    "9": "----.",
    ".": ".-.-.-",
    ",": "--..--",
    "/": "-..-.",
    "?": "..--.."
} 

let chars = Object.keys(MORSE);
// let chars = ["7", "w", "?"];

const NUMBEROFCHARS = chars.length;
  // const NUMBEROFCHARS = 3
  
let charIndeces = [];
let indexPointer = 0;

function initRandomChars() {
  // called by startAssessment() to initialize random characters
  // initialize indeces for the 40 characters
  for (i = 0; i < NUMBEROFCHARS; i++) {
    charIndeces.push(i); 
  }
  // randomize the indeces
  shuffleArray(charIndeces);
  // initialize index pointer
  indexPointer = 0;
};

function selectNextChar() {
  // selects next character to send - randomly

  // returns space after last character
  if (indexPointer == NUMBEROFCHARS) {
    // out of characters - return ""
    return [ "", "" ];
  } 
  // return character and increment pointer
  nextChar = chars[charIndeces[indexPointer]];
  indexPointer ++;
  return [ nextChar, MORSE[nextChar] ]; // returns both the character and the morse translation
};

function shuffleArray(array) {
  let curId = array.length;
  // There remain elements to shuffle
  while (0 !== curId) {
    // Pick a remaining element
    let randId = Math.floor(Math.random() * curId);
    curId -= 1;
    // Swap it with the current element.
    let tmp = array[curId];
    array[curId] = array[randId];
    array[randId] = tmp;
  }
  return array;
}