// borrowed this from Jim Poponea NS8J

var toneAudioContext = null; // assign in calling file
var Sounder = function () {
    var self = this;

    this.rampTime = 0.003;
    this.context = toneAudioContext;
    this.oscillator = this.context.createOscillator();
    this.gainNode = this.context.createGain();
    this.gainNode.gain.setValueAtTime(0, this.context.currentTime);
    this.oscillator.frequency.setValueAtTime(600, this.context.currentTime);
    this.oscillator.connect(this.gainNode);
    this.gainNode.connect(this.context.destination);
    this.oscillator.start(0);
    
}

Sounder.prototype.setFrequency = function (freq) {
    this.oscillator.frequency.setValueAtTime(freq, this.context.currentTime);
}

Sounder.prototype.getTime = function () {
    return this.context.currentTime;
}

Sounder.prototype.beginTone = function (atTime, frequency) {
    this.oscillator.frequency.setValueAtTime(frequency, 0);;
    this.gainNode.gain.setTargetAtTime(1.0, atTime || this.context.currentTime, this.rampTime);
}

Sounder.prototype.endTone = function (atTime) {
    this.gainNode.gain.setTargetAtTime(0.0, atTime || this.context.currentTime, this.rampTime);
}
Sounder.prototype.stopNow = function () {
    this.gainNode.gain.cancelScheduledValues(0.0);
    this.gainNode.gain.setTargetAtTime(0.0, 0, this.rampTime);
}

Sounder.prototype.playTone = function (start, length, frequency) {
    var end = start + length;
    this.beginTone(start, frequency);
    this.endTone(end);
    return end;
}