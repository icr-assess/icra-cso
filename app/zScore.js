// javascript port of: https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/48895639#48895639

function sum(a) {
    return a.reduce((acc, val) => acc + val)
}

function mean(a) {
    return sum(a) / a.length
}

function stddev(arr) {
    const arr_mean = mean(arr)
    const r = function(acc, val) {
        return acc + ((val - arr_mean) * (val - arr_mean))
    }
    return Math.sqrt(arr.reduce(r, 0.0) / arr.length)
}

function smoothed_z_score(y, params) {
    var p = params || {}
    // init cooefficients
    const lag = p.lag || 5
    const threshold = p.threshold || 3.5
    const influence = p.influece || 0.5

    if (y === undefined || y.length < lag + 2) {
        throw ` ## y data array to short(${y.length}) for given lag of ${lag}`
    }
    //console.log(`lag, threshold, influence: ${lag}, ${threshold}, ${influence}`)

    // init variables
    var signals = Array(y.length).fill(0)
    var filteredY = y.slice(0)
    const lead_in = y.slice(0, lag)
    //console.log("1: " + lead_in.toString())

    var avgFilter = []
    avgFilter[lag - 1] = mean(lead_in)
    var stdFilter = []
    stdFilter[lag - 1] = stddev(lead_in)
    //console.log("2: " + stdFilter.toString())

    for (var i = lag; i < y.length; i++) {
        //console.log(`${y[i]}, ${avgFilter[i-1]}, ${threshold}, ${stdFilter[i-1]}`)
        if (Math.abs(y[i] - avgFilter[i - 1]) > (threshold * stdFilter[i - 1])) {
            if (y[i] > avgFilter[i - 1]) {
                signals[i] = +1 // positive signal
            } else {
                signals[i] = -1 // negative signal
            }
            // make influence lower
            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i - 1]
        } else {
            signals[i] = 0 // no signal
            filteredY[i] = y[i]
        }

        // adjust the filters
        const y_lag = filteredY.slice(i - lag, i)
        avgFilter[i] = mean(y_lag)
        stdFilter[i] = stddev(y_lag)
        
    }
    // console.log('avgFilter ', avgFilter);
    // console.log('stdFilter ', stdFilter);
    return signals
}
//  READY TO WALK THROUGH AND TEST
function calcResponseTime(micLevels, elapsedTimes, parameters) {
    // return time in msec to beginning of student's voice response

    // do the math
    signals = smoothed_z_score(micLevels, parameters);

    // check to see if there are no signals
    if (!signals.includes(1)){
        // there are no signals to be found - return zero
        return 0;
    }
    // collect signal runs = signal widths
    runs = [];
    run_length = 0;
    run_start = 0;
    for (let i = 0 ; i < signals.length ; i ++) {
        if ( signals[i] <= 0 ){
            if (run_length > 0){
                // run in progress, just ended, add to runs array
                runs.push([run_start, run_length]);
                run_length = 0;
            } else {
                // no run in progress, no run started - do nothing
            };
        } else {
            // signal must be 1
            if (run_length > 0) {
                // run in progress, continue extending it
                run_length += 1;
            } else {
                // new run started
                run_start = i;
                run_length = 1;
            }
        };
    };

    if (run_length >= 1) {
        // a run was in progress at the end - add it to the list
        runs.push([run_start, run_length]);
    };
    
    // find the maximum length run and use it to calc CALC_TIME
    // console.log(runs);
    const START = 0;
    const LENGTH = 1;
    max_length = 0;
    run_start = 0;
    for (let i = 0; i < runs.length; i ++){
        // console.log(i, runs, runs[i][LENGTH], runs[i][START])
        if (runs[i][LENGTH] > max_length){
            max_length = runs[i][LENGTH];
            run_start = runs[i][START];
        }
    }
    return elapsedTimes[run_start];
};


function firstSignalIndex(levels, parameters) {
    // returns the index of the element of levels corresponding
    // to the first zScore signal
    // returns -1 if no signal
    zScores = smoothed_z_score(levels, parameters);
    return zScores.findIndex((element) => element > 0);
}

function timeToFirstSignal (levels, times, parameters){
    // returns the time at which the first zScore signal occurs
    // returns 0 if no signals
    first = firstSignalIndex(levels, parameters);
    if ( first == -1 ) {
    //   no signals - something is wrong
        return 0;
    } else {
        return times[first];
    };
}

// function timeToValidSignal (levels, times, parameters){
//     // returns the time at which the first zScore signal occurs
//     // returns 0 if no signals
//     first = validSignalIndex(levels, parameters);
//     if ( first == -1 ) {
//     //   no signals - something is wrong
//         return 0;
//     } else {
//         return times[first];
//     };
// }
// function validSignalIndex(levels, parameters) {
//     // finds and returns first positive signal which is not a blip
//     zScores = smoothed_z_score(levels, parameters);
//     levelsLength = levels.length;
//     sliceStart = 0;
//     while ( true ){
//         // answer = prompt('Continue? ', 'yes');
//         if (sliceStart >= levelsLength) {
//             // end of levels - no signal to be found
//             // console.log('sliceStart beyond levels', sliceStart)
//             return 0
//         }
//         signal = zScores.slice(sliceStart, levelsLength).findIndex(
//             (element) => element > 0);
        
//         if ( signal == -1 ) {
//             // no more signals to be found
//             // console.log('no more signals to be found from ', sliceStart)
//             return 0
//         } else {
//             signal += sliceStart // because findIndex gave the index within the slice only
//         }

//         // is this a blip?
//         if (!isBlip(signal, levels)) {
//             // nope
//             // console.log('not a blip ', sliceStart)
//             return signal;
//         } else {
//             // yep, move on
//             // console.log('blip found ', sliceStart)
//             sliceStart = signal + 1;
//         }
//     }
// };

// const BASELINESAMPLES = 3; // # values to use to establish the baseline level
// const SIGNALSAMPLES = 5; // # values to use to establish the signal level
// const SIGNALTHRESHOLD = 0.2; // ratio of signal to baseline level considered to connote a valid signal, vs a blip


// function isBlip(signal, levels)   {
//     // determine whether signal (actually levels[signal]) represents a blip (return true) 
//     // or the student's response (return false) by comparing the 
//     // next few levels vs the baseline

//     // establish baseline
//     if ( signal < BASELINESAMPLES) {
//         // definitely a blip because response time is inhumanly short!
//         return true
//     }
//     baselineLevel = mean(levels.slice(signal - BASELINESAMPLES - 1, signal - 1));

//     // sample signal to estimate strength and duration
//     signalLevel = mean(levels.slice(signal, signal + SIGNALSAMPLES));

//     // compare and return
//     if ( baselineLevel / signalLevel >= SIGNALTHRESHOLD ) {
//         // probably a blip
//         return true
//     } else {
//         // probably a signal
//         return false
//     }
    
// }