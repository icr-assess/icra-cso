// import { smoothed_z_score as zScore } from "./zScore.js"
// import { selectNextChar } from "./morse.js"

let studentName, studentCallsign, studentEmail;
let spkr;
let spkrSel;  // speaker output selector
let speakers; // output choices
let mic;
let sel; // mic input selector
let sources; // input sources
let startButton;
let saveButton; // hopefully temporary
let micLevels = [];
let elapsedTimes = [];
let assessmentData = []; // the data eventually output as json
// data is structured as an array of objects
let tech = {} // an object to contain calib data, etc.

let micTestData = []; // data collected to establish relative mic levels - quiet and speech

let codeOsc, env;
let loopStatus = 'idle';
let syncTime = 0; // time when student input starts for current character, msec since baseSyncTime 
let elapsedTime = 0; // msec since syncTime for each audio level reading
let baseSyncTime = 0; // time first char starts - 
let currentChar = "";

let calibStart;
let calibFile;  // sound file for calibration data
let latency;
let parameters = {"lag": 15, "threshold": 6, "influence": 0.51}; 
  // default params for z-score calc

let levelLoop;
let recorder, soundFile;
// let stopRecording;  // Promise
let initializeSoundFile; // promise

const TRUERESPONSETIME = 0; // placeholder for actual response time as measured in Audacity

const audio = document.createElement('audio');
const SMOOTHING = 1; // smoothing factor for getLevel() (from mic)

const CHARSPACING = 3000; // length of time (msec) between characters
      // this is now for timing out in case student fails to strike a key

const SPACE = 32; // key-code for space bar



function setup() {
  getAudioContext().suspend(); 
  // note: neither speaker nor mic selector is dynamic - they are configured once
  // during setup() and never again. if user is likely to change something in flight,
  // e.g., plug in headphones, need to address that. not needed at this point.

  
  // configure oscillator and envelope for dits and dahs
  codeOsc = new p5.Oscillator(550);
  codeOsc.connect();  
  env = new p5.Envelope(0.004, 1, 0, 1, 0.004, 0);
  codeOsc.amp(env)

  // separate osc for testing speaker?
  testOsc = new p5.Oscillator(550);
  testOsc.connect();  

  // create an empty sound file that we will use to capture the recording for saving
  soundFile = new p5.SoundFile();
  calibFile = new p5.SoundFile();

  // // create a sound recorder
  // recorder = new p5.SoundRecorder();
  // recorder.setInput(mic);
 
  identButton = select('#begin');
  identButton.mouseClicked(storeID);
  
  audioStartButton = select('#audioStart');
  audioStartButton.mouseClicked(startAudio);

  testSpkrButton = select('#spkrTest');
  testSpkrButton.mouseClicked(testSpeaker);
  
  okaySpkrButton = select('#spkrOkay');
  okaySpkrButton.mouseClicked(okaySpeaker);

  testMicButton = select('#micTest');
  testMicButton.mouseClicked(testMic);
  
  calibrateButton = select('#calib');
  calibrateButton.mouseClicked(calibrate);

  // button to start assessment
  startButton = select('#Start');
  startButton.mouseClicked(startAssessment);

  // button to finish (i.e., save files)
  finishButton = select('#Finish');
  finishButton.mouseClicked(finishAssessment);
  };

function draw(){
  // console.log(loopStatus, currentChar);
  switch (loopStatus) {
    case "idle":
      // do nothing
      break;

    case "testingMic":
      // establish quiet and speaking levels
      
      micLevel = mic.getLevel(SMOOTHING);
      micTestData.push(micLevel);      
      break;

    case "calibrating":
      // determining latencies for recording and mic levels

      micLevel = mic.getLevel(SMOOTHING);
      micLevels.push(micLevel)
      elapsedTime = Date.now() - calibStart;
      elapsedTimes.push(elapsedTime)
      // calibAudioData.push([calibTime, micLevel]);
      
      break;

    case "sendingChar":
      // character being sent
      // no code needed here  
      break;

    case "studentResponding":
      // keep storing mic levels
      // console.log('start storing mic levels ', Date.now());
      micLevel = mic.getLevel(SMOOTHING);
      elapsedTime = Date.now() - baseSyncTime - syncTime - latency;
      if( elapsedTime >= 0 ) {
        // only include times after latency period, i.e., non-negative
        micLevels.push(micLevel);
        elapsedTimes.push(elapsedTime)
      } else {
        // console.log('deleted time/level:  ', elapsedTime, micLevel);
      };
      break;

    case "error":
      console.log('something went wrong');
      break;

    default:
      console.log(loopStatus, "error, shouldn't have gotten here");
      break;
  };
};

function recordSoundFile(){
  recorder.record(soundFile);
};

function saveSoundFile() {
  saveSound(soundFile, 'icraSound.wav');
};

function startAssessment () {
  startButton.elt.setAttribute('disabled', true);  // so space bar can't restart assessment
  // console.log('entering startAssessment');
  // start recording
  // getAudioContext().resume();
  // console.log('assess: audio context resumed');
    
  mic.setSource(sel.value());
  // console.log(mic);
  
    // start mic and start recording to soundFile
  mic.start(recordSoundFile);  
  // mic will remain running for entire session

  // line up first character
  initRandomChars();
  [ currentChar, currentCharMorse ] = selectNextChar(); // assume it's not " "
 
  // initialize levels, times and assessmentData to []
  micLevels = [];
  elapsedTimes = [];
  assessmentData = [];

  // change loop status to "sendingChar"
  loopStatus = "sendingChar"
  
  // establish baseSyncTime
  baseSyncTime = Date.now();

  // send next character;
  sendMorse(currentCharMorse)
  // console.log('after sendMorse')
  // console.log('leaving startAssessment')
};

function sendNextChar (charTyped="") {
  // console.log('entering sendNextChar with:', charTyped)
  // clear student response timer
  clearTimeout(nextCharTime);

  if ( charTyped == "") {
    // student response timed out without the student typing in a character
    responseTime = CHARSPACING;
    console.log('No student keystroke for this character: ', currentChar)
  } else {
    // determine response time
    responseTime = calcResponseTime(micLevels, elapsedTimes, parameters)
    // console.log("response time; ", responseTime, "for ", currentChar);
  };

  // append data from current character to assessmentData
  // note: TRUERESPONSETIME is a placeholder for response time measured in Audacity
  assessmentData.push({
    trueChar: currentChar,
    syncTime: syncTime,
    responseTime: responseTime,
    charTyped: charTyped, 
    trueResponseTime: 0,  // placeholder
    elapsedTimes: elapsedTimes,
    micLevels: micLevels
  });
  // console.log('data pushed');
  
  // initialize studentAudioData for next char
  // studentAudioData = [];
  micLevels = [];
  elapsedTimes = [];
  // console.log('audio data reset')

  [ currentChar, currentCharMorse ] = selectNextChar();
 
  if ( currentChar == "" ) {
    // we're done, wrap it up
    // stop recorder, and send the result to soundFile
    recorder.stop();

        // turn off mic
    mic.stop();

    // loop status to idle
    loopStatus = "idle";
    // console.log('Done');

    // compile stats and combine with assessmentData
    stats = analyseICRAData(assessmentData);
    identData.end = new Date();
    results = {
      identification: identData,
      assessment: assessmentData,
      statistics: stats,
      technical: tech
    };
  
    // notify student that assessment is complete
    icraComplete = createSpan("Your ICR Assessment is complete");
    icraComplete.parent('#icraComplete');
    icraComplete.elt.style.fontWeight = 'bold';
    icraComplete.elt.style.color = "red";
    // enable finish elements
    finishButton.removeAttribute('disabled');
    // disable assessment start elements
    startButton.elt.setAttribute('disabled', true);   
  } else {
    // more characters, keep going

    // loop status to "sendingChar"
    loopStatus = "sendingChar";

    // sendMorse (next char)
    sendCharDelay = setTimeout(sendMorse, 1000); // delay before sending next character
    // console.log('leaving sendNextChar after: ', currentChar)
  };
};

function finishAssessment(){
//
    // write assessmentData to json file
    saveJSON(results,'icraData.json');
    saveSound(soundFile, 'icraSound.wav');
    
    // thank student for completing the assessment
    finishICRA = createSpan("Thanks for completing your ICR Assessment!!");
    finishICRA.parent('#finishICRA');
    finishICRA.elt.style.fontWeight = 'bold';
    finishICRA.elt.style.color = "red";
      
};

function sendDit () {
  // fires codeOsc for 48 msec = dit at 25 wpm
  // console.log('sendDit time ', Date.now())
  codeOsc.start();
  env.triggerAttack(codeOsc, 0);
  env.triggerRelease(codeOsc, 0.044); // 48 msec minus 4 msec release
  // console.log('End dit ',Date.now());
}

function sendDah () {
  // fires codeOsc for 144 msec = dah at 25 wpm
  // console.log("sendDah time ", Date.now())
  codeOsc.start();
  env.triggerAttack(codeOsc, 0);
  env.triggerRelease(codeOsc, 0.14); // 144 msec minus 4 msec release
}

function sendMorse (elements=currentCharMorse) {
// sends one character in morse code at 25 wpm
// delay is the msec to wait before sending the next element - it includes an element space
// the delay starts at zero so the first element is sent right away.
// sendMorseStartTime = Date.now();
  delay = 0;
  for (let i = 0; i < elements.length; i++) {
    element = elements[i];
    // console.log('delay ', delay);
    // console.log(element);
    if (element == '.') {
      // dit
      setTimeout(sendDit, delay);
      delay += 96; // dit plus element space
    } else if (element == '-') {
      // dah
      setTimeout(sendDah, delay);
      delay += 192; // dah plus element space
    } else {
      // error
      console.log('something went wrong!!');
    }
  }
  delay -= 48; // subtract last element space
  // console.log('delay ', delay);
  // console.log(Date.now());
  setTimeout(charSent, delay);
  // console.log('sendMorseElapsedTime ',Date.now()-sendMorseStartTime);
};

function charSent() {
  // if first character, initialize sync timer
  // console.log('char sent ', Date.now());
  // calculate sync time 
  syncTime = Date.now() - baseSyncTime; 
  // console.log('character sent time ', Date.now());
  
  // set response time-out (eventually use listener)
  nextCharTime = setTimeout(sendNextChar, CHARSPACING);

  // initialize audioData to []
  // shouldn't have to do this - is done in draw loop
  // console.log(Date.now());
  loopStatus = 'studentResponding';

  // levelLoop = setInterval(getMicLevels, 6);
  
};

function testSpeaker() {
  // for user to test speakers and set level
  getAudioContext().resume();
  // console.log('testSpeaker: audio context resumed')
  userStartAudio();
  // console.log('testSpeaker: user audio started')
  audio.setSinkId(speakers[spkrSel.value()].deviceId)
    .then (testOsc.start());
};

function okaySpeaker(){
  testOsc.stop();
  // console.log(select("#spkrReady"));
  // notify student that speaker is ready
  spkrReady = createSpan("Speaker is ready");
  spkrReady.parent('#spkrReady');
  spkrReady.elt.style.fontWeight = 'bold';
  spkrReady.elt.style.color = "red";
  // enable mic test elements
  testMicButton.removeAttribute('disabled');
  sel.removeAttribute('disabled');
  // disable speaker test elements
  spkrSel.elt.setAttribute('disabled', true);
  testSpkrButton.elt.setAttribute('disabled', true);
  okaySpkrButton.elt.setAttribute('disabled', true);
}

function testMic() {
  // turn off osc - test done
  getAudioContext().resume(); 
  // console.log('testMic: audio context resumed')
  mic.setSource(sel.value());
  mic.start();

  // create a sound recorder
  recorder = new p5.SoundRecorder();
  recorder.setInput(mic);
  
  micTestData = [];
  loopStatus = 'testingMic'
  setTimeout(micTestDone, 2000);
  
};

function micTestDone() {
  loopStatus = 'idle';
  index = firstSignalIndex(micTestData, parameters);
  if ( index == -1 ) {
    // failed test
    alert('Mic test failed. Check mic and try again');
    console.log('micTest: failed - no signal detected')
    return;
  };
  // now determine whether levels are reasonable -
  quietLevel = mean(micTestData.slice(0,index));
  voiceLevel = mean(micTestData.slice(index, index + 500 / 16));
  // 500 msec / 16 msec per array element - averaging the next 
  // half second after the signal
  if ( quietLevel / voiceLevel > 0.3) {
    alert('Mic test failed. Check mic and try again');
    console.log('mic Test failed: insufficient signal', quietLevel, voiceLevel)
    return;
  };
  console.log(select("#micLevelReady"));
  // notify student that speaker is ready
  micLevelReady = createSpan("Mic level passed");
  micLevelReady.parent('#micLevelReady');
  micLevelReady.elt.style.fontWeight = 'bold';
  micLevelReady.elt.style.color = 'red'
  // enable mic calibration element
  calibrateButton.removeAttribute('disabled');
  // disable mic test elements
  testMicButton.elt.setAttribute('disabled', true);
  sel.elt.setAttribute('disabled', true);
  // console.log( voiceLevel / quietLevel);  
  tech.micQuiet = quietLevel;
  tech.micVoice = voiceLevel;
  tech.micRange = voiceLevel / quietLevel;
};

function calibrate() {
  // determine latencies for recorder and mic levels

  // start recorder and mic - note time
  getAudioContext().resume(); 
  // console.log('calibrate: audio context resumed')
  mic.setSource(sel.value());
  mic.start(recordCalibFile);
  micLevels = [];
  elapsedTimes = [];
  loopStatus = 'calibrating';
  calibStart = Date.now();

  // send 500 msec of silence then send spike (dit for now)
  setTimeout(sendDit, 500);

  // send 500 msec of silence
  setTimeout(doneCalib, 1000);
  // stop recorder and mic
};

function doneCalib() {
  // save data and go back to idle

  // stop recorder
  recorder.stop();

  // turn off mic
  mic.stop();
  latency = timeToFirstSignal(micLevels, elapsedTimes, parameters)
  if (latency == 0) {
    alert("Calibration failed. Increase speaker volume or mic gain and try again");
    console.log('calibrate: failed - no signal detected');
    loopStatus = "idle";  // and wait for student press calib button again
  } else {
    latency = latency - 500;
    // console.log(latency);

    // store latency for future reference
    tech.latency = latency;

    // loop status to idle
    loopStatus = "idle";
    // notify student that mic calib passed
    micCalib = createSpan("Mic calibration passed");
    micCalib.parent('#micCalib');
    micCalib.elt.style.fontWeight = 'bold';
    micCalib.elt.style.color = 'red'
    // console.log('Calibration Done');
    // enable assessment start button
    startButton.removeAttribute('disabled');
    // disable mic calibration element
    calibrateButton.elt.setAttribute('disabled', true);
    // enable assessment start button
    startButton.removeAttribute('disabled');
 };
}

function recordCalibFile() {
  recorder.record(calibFile);
}

function saveCalibFile() {
  saveSound(calibFile, 'calibSound.wav');
};


function keyTyped() {
  if ( loopStatus == 'studentResponding' ) {
    // normal case - student hits key to id cw character  
    // clear next character timer
    clearTimeout(nextCharTime);
    
    sendNextChar(key)
      }           
  // any other time a key is struck, ignore
};



// function keyPressed() {
//   if ( loopStatus == 'studentResponding' ) {
//     // normal case - student hits key to id cw character  
//     // clear next character timer
//     clearTimeout(nextCharTime);
    
//     // console.log("Key struck = <", key, ">  //  Key code = ", keyCode);

//     if (keyCode == 32) {
//       // student hit spacebar to show mistake - send space to sendNextChar
//       sendNextChar(' ')
//     } else {
//       // non-space char - send that value to sendNextChar
//       sendNextChar(key);
//     };
//   }           
//   // any other time a key is struck, ignore
// };

function startAudio() {
  userStartAudio();
  // getAudioContext();

  //  confirm enumerateDevices() is supported
  if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
    console.log("enumerateDevices() not supported.");
    loopStatus = 'error';
    return;
  }
  // configure speaker selector
  navigator.mediaDevices.getUserMedia({audio: true, video: false})
  .then(s => {
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
      // console.log("line 536: ",devices)
      speakers = [];
      devices.forEach(device => {
        // create speakers array with only audiooutput devices
        if (device.kind === 'audiooutput') {
          speakers.push(device);
        }
        // console.log("Line 542 ",speakers)
      })
      // console.log(speakers)
      spkrSel = select('#spkrs');
      // enable speaker test elements
      spkrSel.removeAttribute('disabled');
      testSpkrButton.elt.removeAttribute('disabled');
      okaySpkrButton.elt.removeAttribute('disabled');
      // populate speaker selector
      for (let i = 0; i < speakers.length ; i++) {
        spkrSel.option(speakers[i].label, i);
        };
      // configure mic
      mic = new p5.AudioIn();
      sources = mic.getSources()
        .then(function (result) {
          sel = select('#mics');
          for (let i = 0; i < result.length; i++) {
            sel.option(result[i].label, i);
          };
        })
        .catch(error => {
          console.log('Error: ', error)
        });
      }) 
    .catch(error => {
      console.lot('Error: ', error);
    })
  })
  .catch(error => {
    console.log('Error: ', error)
  });
     
  // configure mic
  mic = new p5.AudioIn();
  sources = mic.getSources()
    .then(function (result) {
      sel = select('#mics');
      for (let i = 0; i < result.length; i++) {
        sel.option(result[i].label, i);
      };
    })
    .catch(error => {
      console.log('Error: ', error)
    });
  
    // notify student that audio system started
  startAudio = createSpan("Audio system started");
  startAudio.parent('#started');
  startAudio.elt.style.fontWeight = 'bold';
  startAudio.elt.style.color = 'red';
  // console.log('Audio system started');

  // disable audio start button
  audioStartButton.elt.setAttribute('disabled', true);
 };

function storeID() {
  // store id info
  studentName = select("#fullName").value();
  studentCallsign = select("#callSign").value();
  studentEmail = select("#email").value();
  assessStart = new Date();
  identData = {
    name: studentName,
    callSign: studentCallsign,
    email: studentEmail,
    start: assessStart
  };
  // console.log("Name: ", studentName, "Call: ", studentCallsign, "eMail: ", studentEmail);
  
  // notify student that id info stored
  startAudio = createSpan("ID info stored");
  startAudio.parent('#identDone');
  startAudio.elt.style.fontWeight = 'bold';
  startAudio.elt.style.color = 'red';
  // console.log('ID info stored');

  // disable id info
  identButton.elt.setAttribute('disabled', true);
  select("#fullName").elt.setAttribute("disabled", true);
  select("#callSign").elt.setAttribute("disabled", true);
  select("#email").elt.setAttribute("disabled", true);
  
  // enable audio start
  audioStartButton.elt.removeAttribute('disabled');

}