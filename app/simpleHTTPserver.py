from http.server import HTTPServer, SimpleHTTPRequestHandler
import os

server_address = ('0.0.0.0', 5000)    
httpd = HTTPServer(server_address, SimpleHTTPRequestHandler)
os.chdir('C:/Repos/icra-cso/app/')  # optional
print('Running server...')
httpd.serve_forever()