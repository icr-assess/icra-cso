ICR stands for Instant Character Recognition, an important skill in improving CW (Morse Code) skills.

ICRA-CSO is a tool to allow cw students and advisors to assess and improve the student's ICR skill level.

ICRA-CSO predecessor, ICRA, used Python on the server side to do quite a bit of the heavy lifting. However now that I understand JS better, I think it makes sense to put most of the functionality on the client side, using JS. Hence, the CSO part of the name stands for "Client-Side Only." My hope is that the new architecture gets me where I want to go more readily and ends up being easier to deploy and maintain. We'll see!